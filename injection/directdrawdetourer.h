class DirectDrawDetourer {
public:
	static struct DirectDrawFunctions {
		HRESULT(STDMETHODCALLTYPE *CreateSurface)(IDirectDraw FAR *,
			LPDDSURFACEDESC, LPDIRECTDRAWSURFACE FAR *, IUnknown FAR *);
		HRESULT(STDMETHODCALLTYPE *SetCooperativeLevel)(IDirectDraw FAR *, HWND, DWORD);
		HRESULT(STDMETHODCALLTYPE *SetDisplayMode)(IDirectDraw FAR *, DWORD, DWORD, DWORD);
		HRESULT(STDMETHODCALLTYPE *SurfaceBlt)(IDirectDrawSurface FAR *, LPRECT, LPDIRECTDRAWSURFACE, LPRECT, DWORD, LPDDBLTFX);
		HRESULT(STDMETHODCALLTYPE *SurfaceUnlock)(IDirectDrawSurface FAR *, LPVOID);
		ULONG(STDMETHODCALLTYPE *SurfaceRelease)(IDirectDrawSurface FAR *);
	} FUNCTIONS;

	DirectDrawDetourer();
	~DirectDrawDetourer();
private:
	void updateDirectDrawFunctions();

	void installDetours();
	void removeDetours();
};

class Detour {
public:
	static struct ContinuumData {
		int gameWidth, gameHeight;
		IDirectDrawSurface* primarySurface;
		IDirectDrawSurface* backBuffer;
	} Data;

	static void ResetData() {
		Data.gameWidth = 0;
		Data.gameHeight = 0;
		Data.primarySurface = nullptr;
		Data.backBuffer = nullptr;
	}

	static HRESULT STDMETHODCALLTYPE CreateSurface(IDirectDraw FAR * flpDD, LPDDSURFACEDESC lpDDSurfaceDesc, LPDIRECTDRAWSURFACE FAR *lplpDDSurface, IUnknown FAR *pUnkOuter);
	static HRESULT STDMETHODCALLTYPE SetCooperativeLevel(IDirectDraw FAR * flpDD, HWND hwnd, DWORD flags);
	static HRESULT STDMETHODCALLTYPE SetDisplayMode(IDirectDraw FAR * flpDD, DWORD width, DWORD height, DWORD BPP);
	static HRESULT STDMETHODCALLTYPE SurfaceUnlock(IDirectDrawSurface FAR * flpDDS, LPVOID ptr);
	static HRESULT STDMETHODCALLTYPE SurfaceBlt(IDirectDrawSurface FAR * flpDDS, LPRECT lpDestRect, LPDIRECTDRAWSURFACE lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx);
	static ULONG STDMETHODCALLTYPE SurfaceRelease(IDirectDrawSurface FAR * flpDDS);
};