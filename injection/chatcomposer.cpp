#include "stdafx.h"
#include "globals.h"

#ifdef USE_CUSTOM_CHAT
BOOL(WINAPI *OriginalPeekMessageA)(LPMSG, HWND, UINT, UINT, UINT) = PeekMessageA;
BOOL WINAPI PeekMessageADetour(LPMSG msg, HWND hwnd, UINT filtermin, UINT filtermax, UINT remove) {
	BOOL hasmsg = OriginalPeekMessageA(msg, hwnd, filtermin, filtermax, remove);
	bool skip = false; // TODO: text expansion feature?
	if (hasmsg) {
		// Special keys (VK_ESCAPE, VK_F11 etc), these aren't sent in WM_CHAR
		if (msg->message == WM_KEYDOWN) {
			if (msg->wParam == VK_ESCAPE) {
				char vkchar = static_cast<char>(LOWORD(MapVirtualKeyA(msg->wParam, MAPVK_VK_TO_CHAR)));
#ifdef _DEBUG
				printf("Pressed a vkey: %i, %c\n", msg->wParam, vkchar);
#endif
				chatcomposer->handleInput(vkchar);
			}
		}

		// For macros, no WM_CHAR message is received (i.e. for Ctrl+A, no 'a' WM_CHAR message)
		if (msg->message == WM_CHAR) {
#ifdef _DEBUG
			printf("Got wm_char: %c (%i)\n", msg->wParam, msg->wParam);
#endif
			chatcomposer->handleInput(msg->wParam);
		}
	}

	return hasmsg && !skip;
}

chat::Composer::Composer() :
menuVisible(false),
messagerIndex(0),
current()
{
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)OriginalPeekMessageA, PeekMessageADetour);
	DetourTransactionCommit();
}

chat::Composer::~Composer() {
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourDetach(&(PVOID&)OriginalPeekMessageA, PeekMessageADetour);
	DetourTransactionCommit();
}

void synthesizeInput(std::string text) { // use \b and \n for backspace and enter
	std::vector<INPUT> inputs;

	INPUT shiftDown = {}, shiftUp = {};
	shiftDown.type = INPUT_KEYBOARD;
	shiftDown.ki.wVk = VK_SHIFT;
	shiftUp.type = INPUT_KEYBOARD;
	shiftUp.ki.wVk = VK_SHIFT;
	shiftUp.ki.dwFlags = KEYEVENTF_KEYUP;

	for (auto fakec : text) {
		short vkstate = VkKeyScanA(fakec);
		bool useShift = (vkstate & 0x100) != 0;
		if (useShift)
			inputs.push_back(shiftDown);

		INPUT i = {};
		i.type = INPUT_KEYBOARD;
		i.ki.wVk = 0xFF & vkstate;
		 
		inputs.push_back(i);
		i.ki.dwFlags = KEYEVENTF_KEYUP;
		inputs.push_back(i);

		if (useShift)
			inputs.push_back(shiftUp);
	}

	SendInput(inputs.size(), inputs.data(), sizeof(INPUT));
}

std::string getClipboardText() {
	// return empty string if clipboard does not contain text or fails to open 
	if (!IsClipboardFormatAvailable(CF_TEXT) || !OpenClipboard(NULL))
		return std::string(); 

	std::string clipboardText;
	HANDLE clipboardData = GetClipboardData(CF_TEXT);
	if (clipboardData) {
		void* p = GlobalLock(clipboardData);
		if (p) {
			clipboardText = reinterpret_cast<char*>(p);
			GlobalUnlock(clipboardData);
		}
	}

	CloseClipboard();
	return clipboardText;
}

chat::Message::Type getMessageType(char first) {
	chat::Message::Type type = chat::Message::Type::Other;
	switch (first) {
	case '\'':
		type = chat::Message::Type::Team;
		break;
	case ':':
	case '/':
		type = chat::Message::Type::Private;
		break;
	case ';':
		type = chat::Message::Type::Channel;
		break;
	case '\"':
		type = chat::Message::Type::Freq;
		break;
	default: 
		type = chat::Message::Type::Public;
	}

	return type;
}

#define CHAR_BACKSPACE	0x08
#define CHAR_LINEFEED	0x0A
#define CHAR_ESCAPE		0x1B
#define CHAR_TAB		0x09
#define CHAR_ENTER		0x0D // carriage return	
#define CHAR_CTRLV		0x16
const std::regex chat::Composer::PrivateMessageRegex = std::regex(R"::(^:[^:]*::)::");
/**
 * This isn't accurate, better to use a state machine (since pressing escape while banner selection or arena selection
 * is going on does not result in the menu being visible. Probably similar case with when F1 help is open.
 */
void chat::Composer::handleInput(char c) {
	switch (c) {
	case CHAR_BACKSPACE:
		if (!current.raw.empty())
			current.raw.resize(current.raw.size() - 1); // erase the last char by resizing the string
		break;
	case CHAR_LINEFEED: // do nothing for linefeed and tab
	case CHAR_TAB: 
		break;
	case CHAR_ESCAPE: // escape
		menuVisible = !menuVisible;
		break;
	case CHAR_ENTER:
		if (menuVisible)
			menuVisible = false;
		current = {};
		break;
	case CHAR_CTRLV:
		if (menuVisible)
			menuVisible = false;
		else {
			current.raw += getClipboardText();
			current.type = getMessageType(current.raw.at(0));
		}
		break;
	default:
		if (menuVisible) {
			switch (std::tolower(c)) {
			case 'a': // arena list
			case 'b': // banner
				return;

			case 'f': // show fps
			case 'i': 
			case 'q': // quit game
			case 's': // spec from play
				menuVisible = false;
				return;

			default:
				if (c >= '1' && c <= '9') { // ship selection
					menuVisible = false;
					return;
				}

				break;
			}
		}

		current.raw += c;

		if (current.raw.size() == 1)
			current.type = getMessageType(c);
		else if (c == ':') { // length > 1
			int replaceCount = 0;
			std::smatch match;
			if (current.raw == "::") {
				messagerIndex = 0;
				replaceCount = 2;
			} else if (std::regex_match(current.raw, match, PrivateMessageRegex)) { // :<name>::
				messagerIndex = (messagerIndex + 1) % 5;
				replaceCount = match[0].str().size();
			}

			if (replaceCount != 0) {
				std::stringstream replacement;
				replacement << ":" << memory.GetMessagerFromHistory(messagerIndex) << ":";
				current.raw.replace(0, replaceCount, replacement.str());
			}
		}
		break;
	}

	// update the message content
	current.content = current.raw;
}

chat::Message chat::Composer::getMessage() {
	return current;
}

bool chat::Composer::isMenuVisible() {
	return menuVisible;
}
#endif // USE_CUSTOM_CHAT