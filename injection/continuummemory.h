#pragma once
class ContinuumMemory {
private:
	DWORD historyAddr;
	void InitMessagerHistoryAddress();
public:
	ContinuumMemory();
	std::string GetMessagerFromHistory(int index);
	bool SetCustomResolution(int customResolutionMenuIndex, int resX, int resY);
};