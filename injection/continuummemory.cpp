#include "stdafx.h"
#include "continuummemory.h"

ContinuumMemory::ContinuumMemory() {
	InitMessagerHistoryAddress();
}

#define MENU_DLL_NAME			TEXT("menu040.dll")
DWORD GetMenu040DLLBase() {
	HANDLE continuumProc = GetCurrentProcess();
	HMODULE menu040DLL = NULL;

	HMODULE buffer[10];
	DWORD modulesReturnedBytes;
	EnumProcessModules(continuumProc, buffer, sizeof(buffer), &modulesReturnedBytes);
	unsigned int numModules = modulesReturnedBytes / sizeof(HMODULE);
	HMODULE* loadedModules = new HMODULE[numModules];
	EnumProcessModules(continuumProc, loadedModules, numModules * sizeof(HMODULE), &modulesReturnedBytes);

	for (unsigned int i = 0; i < numModules; i++) {
		TCHAR filename[128];
		GetModuleBaseName(continuumProc, loadedModules[i], filename, 128);

		if (wcscmp(MENU_DLL_NAME, filename) == 0) {
			menu040DLL = loadedModules[i];
			break;
		}
	}

	if (menu040DLL) {
		MODULEINFO mi;
		GetModuleInformation(continuumProc, menu040DLL, &mi, sizeof(mi));
		return (DWORD)mi.lpBaseOfDll;
	}

	return NULL;
}

#define OPTIONS_BASE_OFFSET		0x47F98
#define MESSAGE_HISTORY_OFFSET	0x205c
void ContinuumMemory::InitMessagerHistoryAddress() {
	historyAddr = NULL;
	DWORD menuBase = GetMenu040DLLBase();
	if (menuBase) {
		DWORD optionsAddr = *reinterpret_cast<DWORD*>(menuBase + OPTIONS_BASE_OFFSET);
		historyAddr = optionsAddr + MESSAGE_HISTORY_OFFSET;
	}
}

#define MESSAGER_HISTORY_NAME_LENGTH	24	// each name is max 24 chars (12 bytes)
std::string ContinuumMemory::GetMessagerFromHistory(int index) {
	// Continuum stores last 5 players to private message
	if (!historyAddr || !(index >= 0 && index < 5)) 
		return std::string();

	char* players = reinterpret_cast<char*>(historyAddr);
	std::string name(players + index * MESSAGER_HISTORY_NAME_LENGTH);
	return name;
}

// Starting memory address of array listing Resolutions in menu040.dll
#define RESX_OFFSET 0x441D8 
#define RESY_OFFSET 0x441DC 
#define MENU_SELECTED_VAL_OFFSET 1000
bool ContinuumMemory::SetCustomResolution(int customResolutionMenuIndex, int resX, int resY) {
	__try {
		DWORD menuBase = GetMenu040DLLBase();
		int resolutionListOffset = 0x020 * (customResolutionMenuIndex - MENU_SELECTED_VAL_OFFSET);

		DWORD* resEntryX = (DWORD*)(menuBase + RESX_OFFSET + resolutionListOffset);
		DWORD* resEntryY = (DWORD*)(menuBase + RESY_OFFSET + resolutionListOffset);

		*resEntryX = resX;
		*resEntryY = resY;

		return true;
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
		return false;
	}

	return false;
}