#include "stdafx.h"
#include "globals.h"

Detour::ContinuumData Detour::Data = Detour::ContinuumData{};

HRESULT STDMETHODCALLTYPE Detour::CreateSurface(IDirectDraw FAR * flpDD,
	LPDDSURFACEDESC surfdesc, LPDIRECTDRAWSURFACE FAR *lplpDDSurface, IUnknown FAR *pUnkOuter) {

	HRESULT createResult = DirectDrawDetourer::FUNCTIONS.CreateSurface(flpDD, surfdesc, lplpDDSurface, pUnkOuter);
	if (createResult == DD_OK) {
		IDirectDrawSurface* createdSurface = *lplpDDSurface;

		// Continuum creates a primary surface with DDSD_CAPS, dwCaps value of DDSCAPS_PRIMARYSURFACE
		// Every other surface has valid DDSD_CAPS, DDSD_WIDTH, DDSD_HEIGHT, with dwCaps value of DDSCAPS_OFFSCREENPLAIN and DDSCAPS_SYSTEMMEMORY
		if ((surfdesc->dwFlags & DDSD_CAPS) && (surfdesc->ddsCaps.dwCaps & DDSCAPS_PRIMARYSURFACE)) {
			Data.primarySurface = createdSurface;
		} else if (Detour::Data.primarySurface != nullptr && Detour::Data.backBuffer == nullptr) {
			Data.backBuffer = createdSurface; // Backbuffer is always the surface created after the primary surface
			Data.gameWidth = surfdesc->dwWidth;
			Data.gameHeight = surfdesc->dwHeight;

#ifdef _DEBUG
			printf("Game surface width and height: %i %i\n", Data.gameWidth, Data.gameHeight);
			ContinuumMemory memory;
			for (int i = 0; i < 5; i++)
				printf("Chat history: %s \n", memory.GetMessagerFromHistory(i).c_str());

			if (drawer)
				throw std::exception("Creating a drawer when one already exists");
#endif
			drawer = new DirectXDrawer(Data.gameWidth, Data.gameHeight);
		}
	}

	return createResult;
}

ULONG STDMETHODCALLTYPE Detour::SurfaceRelease(IDirectDrawSurface FAR * flpDDS) {
	if (drawer) {
		drawer->DiscardBitmapForSurface(reinterpret_cast<int>(flpDDS));

		if (flpDDS == Data.primarySurface) {
			delete drawer;
			drawer = nullptr;
		}
	}

	return DirectDrawDetourer::FUNCTIONS.SurfaceRelease(flpDDS);
}

HRESULT STDMETHODCALLTYPE Detour::SetCooperativeLevel(IDirectDraw FAR * flpDD, HWND hwnd, DWORD flags) {
	// flags may be DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN, so just force the call to use DDSCL_NORMAL (indicating windowed)
	return DirectDrawDetourer::FUNCTIONS.SetCooperativeLevel(flpDD, hwnd, DDSCL_NORMAL);
}

HRESULT STDMETHODCALLTYPE Detour::SetDisplayMode(IDirectDraw FAR * flpDD, DWORD width, DWORD height, DWORD bpp) {
	// SetDisplayMode isn't available when we haven't set the cooperative level with DDSCL_FULLSCREEN | DDSCL_EXCLUSIVE 
	// flags prior. So just pretend we were able to successfully call it by returning DD_OK.
	return DD_OK;
}

void EnsureSurfaceForDrawer(IDirectDrawSurface* surface) {
	if (!surface)
		throw std::exception("Null pointer parameter in EnsureSurfaceForDrawer");

	int key = reinterpret_cast<int>(surface);

	if (!drawer->SurfaceBitmapExists(key)) {
		DDSURFACEDESC desc = {};
		desc.dwSize = sizeof(DDSURFACEDESC);
		surface->GetSurfaceDesc(&desc);
		drawer->CreateEmptyBitmap(key, desc.dwWidth, desc.dwHeight);
	}
}

HRESULT STDMETHODCALLTYPE Detour::SurfaceUnlock(IDirectDrawSurface FAR * surface, LPVOID ptr) {
	// Unlock the surface so do things with it (it is still locked at this point)
	HRESULT result = DirectDrawDetourer::FUNCTIONS.SurfaceUnlock(surface, ptr);

	if (drawer && surface != Data.primarySurface && surface != Data.backBuffer) {
		int key = reinterpret_cast<int>(surface);

		DDSURFACEDESC desc = {};
		desc.dwSize = sizeof(DDSURFACEDESC);
		surface->Lock(NULL, &desc, DDLOCK_WAIT | DDLOCK_READONLY, NULL);
		drawer->CreateBitmapFromSurface(key, desc.dwWidth, desc.dwHeight, desc.lpSurface, desc.lPitch);
		// Detoured surface->unlock so need to use this or will be stuck in an infinite loop
		DirectDrawDetourer::FUNCTIONS.SurfaceUnlock(surface, NULL); 
	}

	return result;
}

HRESULT STDMETHODCALLTYPE Detour::SurfaceBlt(IDirectDrawSurface FAR * destinationSurface, LPRECT destRect, LPDIRECTDRAWSURFACE sourceSurface, LPRECT srcRect, DWORD flags, LPDDBLTFX bltEffects) {
	DWORD fillColor = 0;

	if (drawer) {
		if (destinationSurface == Data.primarySurface || destinationSurface == Data.backBuffer) {
			if (sourceSurface == Data.backBuffer) {
				drawer->Present();
			} else {
				if (flags & DDBLT_COLORFILL) {
					fillColor = bltEffects->dwFillColor;
					if (fillColor == 0xFF000000 && (destRect->right - destRect->left) == Data.gameWidth && (destRect->bottom - destRect->top) == Data.gameHeight)
						drawer->Blank();
					else {
						drawer->ColorFill(D2D1::ColorF(fillColor, 1.f), destRect);
					}
				} else if (sourceSurface) {
					EnsureSurfaceForDrawer(sourceSurface);
					drawer->DrawBitmap(reinterpret_cast<int>(sourceSurface), srcRect, destRect);
#ifdef _DEBUG
				} else {
					printf("Warning! did nothing!\n");
#endif
				}
			}
		} else {
			EnsureSurfaceForDrawer(destinationSurface);

			if (flags & DDBLT_COLORFILL) {
				fillColor = bltEffects->dwFillColor;
				drawer->ColorFillBitmap(reinterpret_cast<int>(destinationSurface), D2D1::ColorF(fillColor, 1.f), destRect);

				if (fillColor == 0 || fillColor == 0xFF000000) // Continuum clears with black
					drawer->ClearBlackInBitmap(reinterpret_cast<int>(destinationSurface));
			} else if (sourceSurface) {
				EnsureSurfaceForDrawer(sourceSurface);
				drawer->DrawBitmapToBitmap(reinterpret_cast<int>(sourceSurface), srcRect,
					reinterpret_cast<int>(destinationSurface), destRect);
#ifdef _DEBUG
			} else {
				printf("Warning! did nothing!\n");
#endif
			}
		}
	}

	if (flags & DDBLT_COLORFILL && fillColor != 0) // need to actually perform colorfills (for non transparent radar)
		return DirectDrawDetourer::FUNCTIONS.SurfaceBlt(destinationSurface, destRect, sourceSurface, srcRect, flags, bltEffects);

	return DD_OK;
}
