﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Microsoft.Win32;

namespace resizer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            ContinuumPathTextBox.Text = Properties.Settings.Default.ContinuumPath;
        }

        private bool HasValidLaunchPath()
        {
            return (ContinuumPathTextBox.Text != "");
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (!HasValidLaunchPath())
            {
                // Quit the app if the user fails to set a valid launch path
                Environment.Exit(0);
            }

            base.OnClosing(e);
        }

        private void BrowseAction(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Executable Files|*.exe";
            Nullable<bool> dialogResult = dialog.ShowDialog();
            if (dialogResult == true)
            {
                Properties.Settings.Default.ContinuumPath = dialog.FileName;
                ContinuumPathTextBox.Text = dialog.FileName;
            }
        }

        private void FinishAction(object sender, RoutedEventArgs e)
        {
            if (!HasValidLaunchPath()) {
                MessageBox.Show("Please select a valid launch path to Continuum.exe.",
                    "Invalid Launch Path",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            } else {
                Properties.Settings.Default.ContinuumPath = ContinuumPathTextBox.Text;
                Properties.Settings.Default.Save();
                Close();
            }
        }
    }
}
