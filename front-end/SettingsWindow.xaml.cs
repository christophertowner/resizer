﻿using System;
using System.Windows;
using System.Windows.Forms;

namespace resizer
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        int customX, customY;

        public SettingsWindow()
        {
            InitializeComponent();
            resolutionSlider.ValueChanged += OnScaleResolution;
            resolutionSlider.Value = Properties.Settings.Default.ResolutionSlider;
            applyButton.Click += OnApply;
        }

        private void OnScaleResolution(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double fraction = resolutionSlider.Value / resolutionSlider.Maximum;
            double scaleValue = 1 + fraction; // resolution scaling ranges from 1x to 2x
            customX = (int)(Screen.PrimaryScreen.Bounds.Width * scaleValue);
            customY = (int)(Screen.PrimaryScreen.Bounds.Height * scaleValue);

            resolutionText.Text = customX + " x " + customY + " (" + scaleValue.ToString("N2") + "×)";
            // kinda funky cause we scale the image between 2 and 1 when the res is native and double
            // (1 - p) * val1 + p * val2 --> val1 when p = 0, val2 when p = 1
            screenTransform.ScaleX = (1 - fraction) * 2 + fraction * 1;
            screenTransform.ScaleY = (1 - fraction) * 2 + fraction * 1;
        }

        private void OnApply(object sender, RoutedEventArgs e)
        {
            // Save preferences
            Properties.Settings.Default.ResolutionSlider = resolutionSlider.Value;
            Properties.Settings.Default.CustomResolutionX = customX;
            Properties.Settings.Default.CustomResolutionY = customY;
            Properties.Settings.Default.Save();

            Close();
        }
    }
}
